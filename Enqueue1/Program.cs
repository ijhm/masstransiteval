﻿using ConnectLabs.Connect.Interface.Queuing;
using MassTransitLib;
using System;
using System.Threading;

namespace Enqueue1
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new MassTransitClient<MessageEnvelope>();
            client.Configure("rabbitmq://localhost/", "guest", "guest", "connectQ");


            for (int i = 0; i < 50; i++)
            {
                var message = $"Enqueuer 1-{i}";

                Console.WriteLine(message);
                client.Send(new MessageEnvelope { Data = message });
                Thread.Sleep(2000);
            }

            string line = Console.ReadLine();
        }
    }
}
