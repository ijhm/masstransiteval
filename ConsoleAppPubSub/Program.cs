﻿using ConnectLabs.Connect.Interface.Queuing;
using MassTransitLib;
using System;

namespace publisher
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new MassTransitClient<MessageEnvelope>();
            client.Configure("rabbitmq://localhost/", "guest", "guest", "connectQ");

            Console.WriteLine("MassTransit pub/sub demo");
            while (true)
            {
                string line = Console.ReadLine();
                if (string.Compare(line, "X", true) == 0)
                    break;

                var message = new MessageEnvelope
                {
                    Data = line
                };

                client.Publish(message);
            };
        }
    }
}
