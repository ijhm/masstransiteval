﻿using ConnectLabs.Connect.Interface.Queuing;
using MassTransit;
using System;
using System.Threading.Tasks;

namespace MassTransitLib
{
    public class MassTransitConsumer<T> : IConsumer<T> where T:class
    {
        private Action<T> _messageHandler;

        public MassTransitConsumer(Action<T> messageHandler) => _messageHandler = messageHandler;

        public async Task Consume(ConsumeContext<T> context)
        {
            await Task.Run(()=> _messageHandler.Invoke(context.Message));
        }
    }
}
