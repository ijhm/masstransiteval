﻿using ConnectLabs.Connect.Interface.Queuing;
using System;

namespace MassTransitLib
{
    public class MassTransitQueueProvider : IQueueProvider
    {
        private MassTransitClient<MessageEnvelope> _queueClient;

        public string Uri { set; get; }

        public void BeginDequeue(string service, Action<MessageEnvelope> messageHandler, Action<Exception> handleError)
        {
            _queueClient = new MassTransitClientBuilder<MessageEnvelope>()
                .BrokerUri(Uri)
                .UserNameAndPassword("guest","guest")
                .Retries(3)
                .QueueName(service)
                .Durable(true)
                .MessageHandler(messageHandler)
                .ErrorHandler(handleError)
                .Build();
        }

        public void EndDequeue()
        {
            _queueClient.Disconnect();
        }

        public void Enqueue(string service, MessageEnvelope message)
        {
            _queueClient.Send(message);
        }
    }
}
