﻿using MassTransit;
using System;
using System.Threading.Tasks;

namespace MassTransitLib
{
    public class MassTransitFaultConsumer<TMessage> : IConsumer<Fault<TMessage>>
    {
        private Action<Exception> _errorHandler;

        public MassTransitFaultConsumer(Action<Exception> errorHandler)
        {
            _errorHandler = errorHandler;
        }

        public async Task Consume(ConsumeContext<Fault<TMessage>> c)
        {
            await Task.Run(() => _errorHandler.Invoke( new Exception(c.Message.Exceptions[0].Message)));
        }
    }
}
