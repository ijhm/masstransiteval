﻿using MassTransit;
using System;
using System.Threading.Tasks;

namespace MassTransitLib
{
    public class ScheduleNotificationConsumer<TMessage> : IConsumer<IScheduleNotification<TMessage>>
        where TMessage : class
    {
        private readonly Uri _service;

        public ScheduleNotificationConsumer(Uri service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<IScheduleNotification<TMessage>> context)
        {
            await context.ScheduleSend(_service,context.Message.DeliveryTime, context.Message.Message);
        }
    }
}
