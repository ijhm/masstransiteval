﻿using GreenPipes;
using MassTransit;
using System;

namespace MassTransitLib
{
    public class MassTransitClient<TMessage> 
        where TMessage : class
    {
        private IBusControl _bus;
        
        private string _brokerUri;
        private string _queueName;

        public MassTransitClient(){}

        public void Configure(string brokerUri, string userName, string userPassword)
        {
            _brokerUri = brokerUri;

            _bus = Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri(_brokerUri), h =>
                {
                    h.Username(userName);
                    h.Password(userPassword);
                });
            });
            _bus.Start();
        }

        public void Configure(string brokerUri, string userName, string userPassword, string queueName)
        {
            _brokerUri = brokerUri;

            _bus = Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri(_brokerUri), h =>
                {
                    h.Username(userName);
                    h.Password(userPassword);
                });
            });
            _bus.Start();
            EndpointConvention.Map<TMessage>(new Uri($"{_brokerUri}{queueName}"));
        }

        public void Configure(string brokerUri, string userName, string userPassword, int retries, string queueName, bool durable,  Action<TMessage> messageHandler, Action<Exception> handleError)
        {
            _brokerUri = brokerUri;
            _queueName = queueName;

            _bus = Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri(_brokerUri), h =>
                {
                    h.Username(userName);
                    h.Password(userPassword);
                });

                cfg.ReceiveEndpoint(host, queueName, e =>
                {
                    e.UseRetry(r => r.Immediate(retries));
                    e.Consumer(() => new MassTransitConsumer<TMessage>(messageHandler));
                    e.Durable = durable;
                });

                cfg.ReceiveEndpoint(host, $"{_queueName}-errors", e =>
                {
                    e.Consumer(() => new MassTransitFaultConsumer<Exception>(handleError));
                });
            });
            _bus.Start();
            EndpointConvention.Map<TMessage>(new Uri($"{_brokerUri}{queueName}"));
        }

        public void Disconnect()
        {
            _bus.Stop();
        }

        public void Send(TMessage message)
        {
            _bus.Send(message);
        }

        public void Publish(TMessage message)
        {
            _bus.Publish(message, c => c.FaultAddress = new Uri($"{_brokerUri}{_queueName}-errors"));
        }
    }
}
