﻿using MassTransit;
using MassTransit.RabbitMqTransport;
using MassTransitLib;
using System;

namespace Extensions
{
    public static class MassTransitExtensions
    {
        public static void AddConsumer<TMessage>(this IRabbitMqBusFactoryConfigurator cfg, IRabbitMqHost host, string queueName, Action<TMessage> messageHandler)
            where TMessage : class
        {
            cfg.ReceiveEndpoint(host, queueName, e =>
              {
                  e.Consumer(()=>new MassTransitConsumer<TMessage>(m=>messageHandler(m)));
              });
            EndpointConvention.Map<TMessage>(new Uri($"{host.Address}{queueName}"));
        }
        public static void AddMessageScheduler<TMessage,TSchedule>(this IRabbitMqBusFactoryConfigurator cfg, IRabbitMqHost host, string scheduleQueueName, string serviceQueueName)
            where TMessage : class where TSchedule : class
        {
            cfg.ReceiveEndpoint(host, scheduleQueueName, e =>
            {
                e.Consumer(() => new ScheduleNotificationConsumer<TMessage>(new Uri($"{host.Address}{serviceQueueName}")));
            });
            EndpointConvention.Map<TSchedule>(new Uri($"{host.Address}{scheduleQueueName}"));
        }
    }
}

