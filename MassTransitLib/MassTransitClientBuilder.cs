﻿using System;

namespace MassTransitLib
{
    public class MassTransitClientBuilder<TMessage> 
        where TMessage : class
    {
        private string _brokerUri= "rabbitmq://localhost/";
        private string _username;
        private string _password;
        private int _retries =3;
        private string _queueName;
        private bool _durable = false;
        private Action<TMessage> _messageHandler;
        private Action<Exception> _errorHandler;

        public MassTransitClientBuilder<TMessage> BrokerUri(string brokeruri)
        {
            _brokerUri = brokeruri;
            return this;
        }

        public MassTransitClientBuilder<TMessage> UserNameAndPassword(string userName, string password)
        {
            _username = userName;
            _password = password;
            return this;
        }

        public MassTransitClientBuilder<TMessage> Retries(int retries)
        {
            _retries = retries;
            return this;
        }

        public MassTransitClientBuilder<TMessage> QueueName(string queueName)
        {
            _queueName = queueName;
            return this;
        }

        public MassTransitClientBuilder<TMessage> MessageHandler(Action<TMessage> messageHandler)
        {
            _messageHandler = messageHandler;
            return this;
        }

        public MassTransitClientBuilder<TMessage> ErrorHandler(Action<Exception> errorHandler)
        {
            _errorHandler = errorHandler;
            return this;
        }

        public MassTransitClientBuilder<TMessage> Durable(bool durable)
        {
            _durable = durable;
            return this;
        }

        public MassTransitClientBuilder<TMessage> UseConsumer(Type messageType, string queueName)
        {
            return this;
        }

        public MassTransitClientBuilder<TMessage> ScheduleMessage(Type messageType, DateTime queueMessageAtThisTime)
        {
            return this;
        }

        public MassTransitClient<TMessage> Build()
        {
            var client = new MassTransitClient<TMessage>();
            client.Configure(_brokerUri, _username, _password, _retries, _queueName, _durable, _messageHandler, _errorHandler);
            return client;
        }
    }
}
