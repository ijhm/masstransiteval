﻿using System;

namespace MassTransitLib
{
    public interface IScheduleNotification<TMessage>
    {
        DateTime DeliveryTime { get; }
        TMessage Message { get; }
    }
}
