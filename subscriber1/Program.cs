﻿using ConnectLabs.Connect.Interface.Queuing;
using MassTransitLib;
using System;

namespace subscribers
{
    class Program
    {
        static void Main(string[] args)
        {
            MassTransitClient<MessageEnvelope> client = new MassTransitClientBuilder<MessageEnvelope>()
                .BrokerUri("rabbitmq://localhost/")
                .UserNameAndPassword("guest", "guest")
                .Retries(3)
                .Durable(true)
                .QueueName($"connectQ-Andrew")
                .MessageHandler(m => Console.WriteLine($"Andrew RECEIVED : {m.Data}"))
                .ErrorHandler(e => Console.WriteLine($"Andrew ERROR : {e.Message}"))
                .Build();

            Console.WriteLine("Andrew Subscriber");
            string line = Console.ReadLine();
        }
    }
}
