﻿using ConnectLabs.Connect.Interface.Queuing;
using MassTransitLib;
using System;

namespace subscribers
{
    class Program
    {
        static void Main(string[] args)
        {
            MassTransitClient<MessageEnvelope> client = new MassTransitClientBuilder<MessageEnvelope>()
                .BrokerUri("rabbitmq://localhost/")
                .UserNameAndPassword("guest", "guest")
                .Retries(3)
                .Durable(true)
                .QueueName($"connectQ-Michael")
                .MessageHandler(m => Console.WriteLine($"Michael RECEIVED : {m.Data}"))
                .ErrorHandler(e => Console.WriteLine($"Michael ERROR : {e.Message}"))
                .Build();

            Console.WriteLine("Michael Subscriber");
            string line = Console.ReadLine();
        }
    }
}

