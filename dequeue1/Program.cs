﻿using ConnectLabs.Connect.Interface.Queuing;
using MassTransitLib;
using System;

namespace dequeue1
{
    class Program
    {
        static void Main(string[] args)
        {
            var queueClient = new MassTransitClientBuilder<MessageEnvelope>()
                .BrokerUri("rabbitmq://localhost/")
                .UserNameAndPassword("guest", "guest")
                .Retries(3)
                .QueueName("connectQ")
                .Durable(true)
                .MessageHandler(m => Console.WriteLine($"RECEIVED : {m.Data}"))
                .ErrorHandler(e => Console.WriteLine($"ERROR : {e.Message}"))
                .Build();

            Console.WriteLine("Dequeuer");
            string line = Console.ReadLine();
        }
    }
}
