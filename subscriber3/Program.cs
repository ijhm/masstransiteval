﻿using ConnectLabs.Connect.Interface.Queuing;
using MassTransitLib;
using System;

namespace subscriber3
{
    class Program
    {
        static void Main(string[] args)
        {
            MassTransitClient<MessageEnvelope> client = new MassTransitClientBuilder<MessageEnvelope>()
                .BrokerUri("rabbitmq://localhost/")
                .UserNameAndPassword("guest", "guest")
                .Retries(3)
                .Durable(true)
                .QueueName($"connectQ-Cristian")
                .MessageHandler(m => Console.WriteLine($"Cristian RECEIVED : {m.Data}"))
                .ErrorHandler(e => Console.WriteLine($"Cristian ERROR : {e.Message}"))
                .Build();

            Console.WriteLine("Cristian Subscriber");
            string line = Console.ReadLine();
        }
    }
}
