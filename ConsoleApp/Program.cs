﻿using ConnectLabs.Connect.Interface.Queuing;
using System;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            MassTransitLib.MassTransitQueueProvider q = new MassTransitLib.MassTransitQueueProvider
            {
                Uri = "rabbitmq://localhost/"
            };


            q.BeginDequeue("ConnectServiceQ",
                m => Console.WriteLine($"A Received: {m.Data}"),
                f => Console.WriteLine($"A FAULT!!{f.Message}"));

            //q.BeginDequeue("ConnectServiceQ",
            //    m => Console.WriteLine($"B Received: {m.Data}"),
            //    f => Console.WriteLine($"B FAULT!!{f.Message}"));

            //q.BeginDequeue("ConnectServiceQ",
            //    m => Console.WriteLine($"C Received: {m.Data}"),
            //    f => Console.WriteLine($"C FAULT!!{f.Message}"));

            Console.WriteLine("MassTransit queuing demo");
            while (true)
            {
                string line = Console.ReadLine();
                if (string.Compare(line, "X", true) == 0)
                    break;

                var message = new MessageEnvelope
                {
                    Data = line
                };

                q.Enqueue("???????", message);
            };


            q.EndDequeue();
        }
    }
}
