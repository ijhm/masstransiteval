﻿using MassTransit.Scheduling;

namespace ScheduledMessages
{
    partial class Program
    {
        public class RecurringSchedule : DefaultRecurringSchedule
        {
            public RecurringSchedule()
            {
                CronExpression = "0 0/1 * 1/1 * ? *";
            }
        }
 }
}

