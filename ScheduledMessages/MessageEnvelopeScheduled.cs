﻿using ConnectLabs.Connect.Interface.Queuing;
using MassTransitLib;
using System;

namespace ScheduledMessages
{
    class MessageEnvelopeScheduled : IScheduleNotification<MessageEnvelope>
    {
        public DateTime DeliveryTime { get; }

        public MessageEnvelope Message { get; }

        public MessageEnvelopeScheduled(MessageEnvelope message, DateTime deliveryTime)
        {
            DeliveryTime = deliveryTime;
            Message = message;
        }
    }
}

