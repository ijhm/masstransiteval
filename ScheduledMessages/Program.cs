﻿using ConnectLabs.Connect.Interface.Queuing;
using Extensions;
using MassTransit;
using System;

namespace ScheduledMessages
{
    partial class Program
    {
        private static string BrokerUri = "rabbitmq://localhost/";

        static void Main(string[] args)
        {
            var bus = Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri(BrokerUri), h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });

                cfg.UseInMemoryScheduler();

                cfg.AddConsumer<MessageEnvelope>(host, "ConnectQ", m => Console.WriteLine(m.Data));
                cfg.AddMessageScheduler<MessageEnvelope,MessageEnvelopeScheduled>(host, "scheduleMessageQ", "ConnectQ");  
            });

            bus.Start();


            Console.WriteLine("Expecting message in about a minute.......");

            OneOffScheduledMessageViaConsumer(bus);

            // OneOffScheduledViaBus(bus);

            //RecurringMessageScheduleDemo(bus);

            string line = Console.ReadLine();
        }

        private static void OneOffScheduledMessageViaConsumer(IBusControl bus)
        {
            var messageSchedule = new MessageEnvelopeScheduled(
                CreateMessage(), 
                DateTime.Now + TimeSpan.FromMinutes(1.0));

            bus.Send(messageSchedule);
        }

        private static void OneOffScheduledViaBus(IBusControl bus)
        {
            ISendEndpoint schedulerEndpoint = bus.GetSendEndpoint(new Uri($"{BrokerUri}quartz")).Result;

            schedulerEndpoint.ScheduleSend(
                new Uri($"{BrokerUri}ConnectQ"),
                DateTime.Now + TimeSpan.FromMinutes(1.0), 
                CreateMessage());
        }

        private static void RecurringMessageScheduleDemo(IBusControl bus)
        {
            ISendEndpoint schedulerEndpoint = bus.GetSendEndpoint(new Uri($"{BrokerUri}quartz")).Result;

            var scheduledRecurringMessage = schedulerEndpoint.ScheduleRecurringSend(
                    new Uri($"{BrokerUri}connectQ"), 
                    new RecurringSchedule(), 
                    CreateMessage()).Result;
        }

        private static MessageEnvelope CreateMessage()
        {
            return new MessageEnvelope
            {
                Data = $"\n*\n*Hello @ {DateTime.Now}\n*"
            };
        }
    }
}

